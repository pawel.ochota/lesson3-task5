const reverseArray = require('..');

describe('reverseArray', () => {
  it('should returns an array', async () => {
    const result = reverseArray([1]);

    expect(Array.isArray(result)).toBeTruthy();
  });

  it('should not modify original parameter', async () => {
    const array = [1, 2, 3];
    const arrayCopy = [...array];
    reverseArray([...array]);
  
    expect(array).toEqual(arrayCopy);
  });

  it('should returns reversed array', async () => {
    expect(reverseArray([1, 2, 3])).toEqual([3, 2, 1]);
    expect(reverseArray(['test', 2, undefined])).toEqual([undefined, 2, 'test']);
    expect(reverseArray([null, null, true, false, NaN])).toEqual([NaN, false, true, null, null]);
  });
});
